package parcel.application;

import parcel.application.exception.MyntVoucherException;
import parcel.application.exception.ParcelDeliveryCostException;
import parcel.application.request.ParcelRequest;
import parcel.application.response.ParcelResponse;
import parcel.application.response.VoucherResponse;
import parcel.application.voucher.Voucher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ParcelDeliveryCostTest {

    @Autowired
    private ParcelDeliveryCost parcelDeliveryCost;

    @MockBean
    private Voucher voucher;

    @Test
    public void parcel_that_exceeds_50kg_should_throw_an_exception() {
        ParcelRequest parcelRequest = ObjectFactory.invalidParcel();

        Assertions.assertThrows(
                ParcelDeliveryCostException.class,
                () -> parcelDeliveryCost.compute(parcelRequest),
                "Parcel weight exceeds 50kg"
        );
    }

    @Test
    public void parcel_that_exceeds_10kg_should_be_tag_as_heavy_parcel() {
        ParcelRequest parcelRequest = ObjectFactory.heavyParcel();

        ParcelResponse parcelResponse = parcelDeliveryCost.compute(parcelRequest);
        assertThat(parcelResponse.getParcelType()).isEqualTo("Heavy Parcel");
    }

    @Test
    public void parcel_that_volume_is_less_than_1500cm_should_be_tag_as_small_parcel() {
        ParcelRequest parcelRequest = ObjectFactory.smallParcel();

        ParcelResponse parcelResponse = parcelDeliveryCost.compute(parcelRequest);
        assertThat(parcelResponse.getParcelType()).isEqualTo("Small Parcel");
    }

    @Test
    public void parcel_that_volume_is_less_than_2500cm_but_greater_than_1500cm_should_be_tag_as_medium_parcel() {
        ParcelRequest parcelRequest = ObjectFactory.mediumParcel();

        ParcelResponse parcelResponse = parcelDeliveryCost.compute(parcelRequest);
        assertThat(parcelResponse.getParcelType()).isEqualTo("Medium Parcel");
    }

    @Test
    public void parcel_that_volume_exceeds_than_2500cm_should_be_tag_as_large_parcel() {
        ParcelRequest parcelRequest = ObjectFactory.largeParcel();

        ParcelResponse parcelResponse = parcelDeliveryCost.compute(parcelRequest);
        assertThat(parcelResponse.getParcelType()).isEqualTo("Large Parcel");
    }

    @Test
    public void heavy_parcel_delivery_cost_should_be_computed_by_weight_multiply_by_20() {
        ParcelRequest parcelRequest = ObjectFactory.heavyParcel();
        ParcelResponse parcelResponse = parcelDeliveryCost.compute(parcelRequest);

        double expectedDeliveryCost = parcelRequest.getWeight() * 20;
        assertThat(parcelResponse.getDiscountedDeliveryCost()).isEqualTo(expectedDeliveryCost);
    }

    @Test
    public void small_parcel_delivery_cost_should_be_computed_by_volume_multiply_by_03decimal() {
        ParcelRequest parcelRequest = ObjectFactory.smallParcel();
        ParcelResponse parcelResponse = parcelDeliveryCost.compute(parcelRequest);

        double expectedDeliveryCost = parcelRequest.volume() * .03;
        assertThat(parcelResponse.getDiscountedDeliveryCost()).isEqualTo(expectedDeliveryCost);
    }

    @Test
    public void medium_parcel_delivery_cost_should_be_computed_by_volume_multiply_by_04decimal() {
        ParcelRequest parcelRequest = ObjectFactory.mediumParcel();
        ParcelResponse parcelResponse = parcelDeliveryCost.compute(parcelRequest);

        double expectedDeliveryCost = parcelRequest.volume() * .04;
        assertThat(parcelResponse.getDiscountedDeliveryCost()).isEqualTo(expectedDeliveryCost);
    }

    @Test
    public void large_parcel_delivery_cost_should_be_computed_by_volume_multiply_by_05decimal() {
        ParcelRequest parcelRequest = ObjectFactory.largeParcel();
        ParcelResponse parcelResponse = parcelDeliveryCost.compute(parcelRequest);

        double expectedDeliveryCost = parcelRequest.volume() * .05;
        assertThat(parcelResponse.getDiscountedDeliveryCost()).isEqualTo(expectedDeliveryCost);
    }

    @Test
    public void it_should_make_discount_depending_by_the_voucher() {
        ParcelRequest parcelRequest = ObjectFactory.largeParcel();
        parcelRequest.setVoucherCode("MYNT");

        VoucherResponse voucherResponse = ObjectFactory.voucherResponse();
        Mockito.when(voucher.fetch("MYNT")).thenReturn(voucherResponse);

        ParcelResponse parcelResponse = parcelDeliveryCost.compute(parcelRequest);

        double expectedDeliveryCost = (parcelRequest.volume() * .05) - voucherResponse.getDiscount();
        assertThat(parcelResponse.getDiscountedDeliveryCost()).isEqualTo(expectedDeliveryCost);
    }

    @Test
    public void it_should_throw_an_exception_if_voucher_is_already_expired() {
        ParcelRequest parcelRequest = ObjectFactory.largeParcel();
        parcelRequest.setVoucherCode("MYNT");

        VoucherResponse voucherResponse = ObjectFactory.voucherResponse();
        voucherResponse.setExpiry(LocalDate.now().minusDays(1));
        Mockito.when(voucher.fetch("MYNT")).thenReturn(voucherResponse);

        Assertions.assertThrows(
                MyntVoucherException.class,
                () -> parcelDeliveryCost.compute(parcelRequest),
                "Voucher code is already expired"
        );
    }

}
