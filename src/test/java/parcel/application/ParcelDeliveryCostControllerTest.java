package parcel.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import parcel.application.request.ParcelRequest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ParcelDeliveryCostController.class)
public class ParcelDeliveryCostControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ParcelDeliveryCost parcelDeliveryCost;

    @Test
    public void it_should_invoke_parcel_delivery_cost() throws Exception {
        ParcelRequest parcelRequest = ObjectFactory.largeParcel();
        parcelRequest.setWeight(20D);
        ObjectMapper objectMapper = new ObjectMapper();

        Mockito
                .when(parcelDeliveryCost.compute(any(ParcelRequest.class)))
                .thenReturn(ObjectFactory.parcelResponse());

        this.mockMvc.perform(
                    post("/parcel/v1/delivery-cost")
                            .content(objectMapper.writeValueAsString(parcelRequest))
                            .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        Mockito.verify(parcelDeliveryCost).compute(any(ParcelRequest.class));
    }
}
