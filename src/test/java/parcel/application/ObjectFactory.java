package parcel.application;

import parcel.application.request.ParcelRequest;
import parcel.application.response.ParcelResponse;
import parcel.application.response.VoucherResponse;

import java.time.LocalDate;

public class ObjectFactory {

    public static ParcelRequest invalidParcel() {
        ParcelRequest parcelRequest = new ParcelRequest();
        parcelRequest.setWeight(100D);
        return parcelRequest;
    }

    public static ParcelRequest heavyParcel() {
        ParcelRequest parcelRequest = new ParcelRequest();
        parcelRequest.setWeight(20D);
        return parcelRequest;
    }

    public static ParcelRequest smallParcel() {
        // Volume is 1000. 10 * 10 * 10
        ParcelRequest parcelRequest = new ParcelRequest();
        parcelRequest.setWeight(10D);
        parcelRequest.setHeight(10D);
        parcelRequest.setWidth(10D);
        parcelRequest.setLength(10D);
        return parcelRequest;
    }

    public static ParcelRequest mediumParcel() {
        // Volume is 1690. 10 * 13 * 13
        ParcelRequest parcelRequest = new ParcelRequest();
        parcelRequest.setWeight(10D);
        parcelRequest.setHeight(10D);
        parcelRequest.setWidth(13D);
        parcelRequest.setLength(13D);
        return parcelRequest;
    }

    public static ParcelRequest largeParcel() {
        // Volume is 3375. 15 * 15 * 15
        ParcelRequest parcelRequest = new ParcelRequest();
        parcelRequest.setWeight(10D);
        parcelRequest.setHeight(15D);
        parcelRequest.setWidth(15D);
        parcelRequest.setLength(15D);
        return parcelRequest;
    }

    public static VoucherResponse voucherResponse() {
        VoucherResponse voucherResponse = new VoucherResponse();
        voucherResponse.setCode("MYNT");
        voucherResponse.setDiscount(12.25);
        voucherResponse.setExpiry(LocalDate.now().plusDays(1));
        return voucherResponse;
    }

    public static ParcelResponse parcelResponse() {
        ParcelResponse parcelResponse = new ParcelResponse();
        parcelResponse.setVoucherResponse(voucherResponse());
        parcelResponse.setParcelRequest(largeParcel());
        return parcelResponse;
    }
}
