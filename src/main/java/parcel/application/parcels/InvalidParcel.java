package parcel.application.parcels;

import parcel.application.exception.ParcelDeliveryCostException;
import parcel.application.request.ParcelRequest;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class InvalidParcel extends Parcel {

    @Override
    public String name() {
        return "Invalid Parcel";
    }

    @Override
    public boolean validate(ParcelRequest parcelRequest) {
        return parcelRequest.getWeight() > 50;
    }

    @Override
    public double getCost(ParcelRequest parcelRequest) {
        throw new ParcelDeliveryCostException("Parcel weight exceeds 50kg");
    }
}
