package parcel.application.parcels;

import parcel.application.request.ParcelRequest;

public abstract class Parcel {

    public abstract String name();
    public abstract boolean validate(ParcelRequest parcelRequest);
    public abstract double getCost(ParcelRequest parcelRequest);
}
