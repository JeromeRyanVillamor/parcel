package parcel.application.parcels;

import parcel.application.request.ParcelRequest;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class HeavyParcel extends Parcel {

    private double rate = 20;
    private double minimumWeight = 10;

    @Override
    public String name() {
        return "Heavy Parcel";
    }

    @Override
    public boolean validate(ParcelRequest parcelRequest) {
        return parcelRequest.getWeight() > minimumWeight;
    }

    @Override
    public double getCost(ParcelRequest parcelRequest) {
        return rate * parcelRequest.getWeight();
    }

}
