package parcel.application.parcels;

import parcel.application.request.ParcelRequest;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
public class LargeParcel extends Parcel {

    private double rate = 0.05;

    @Override
    public String name() {
        return "Large Parcel";
    }

    @Override
    public boolean validate(ParcelRequest parcelRequest) {
        return true;
    }

    @Override
    public double getCost(ParcelRequest parcelRequest) {
        return parcelRequest.volume() * rate;
    }
}
