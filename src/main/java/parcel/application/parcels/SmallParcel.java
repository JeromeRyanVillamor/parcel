package parcel.application.parcels;

import parcel.application.request.ParcelRequest;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
public class SmallParcel extends Parcel {

    private double rate = 0.03;
    private double maximumVolume = 1500;

    @Override
    public String name() {
        return "Small Parcel";
    }

    @Override
    public boolean validate(ParcelRequest parcelRequest) {
        return parcelRequest.volume() < maximumVolume;
    }

    @Override
    public double getCost(ParcelRequest parcelRequest) {
        return parcelRequest.volume() * rate;
    }
}
