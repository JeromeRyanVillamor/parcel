package parcel.application.parcels;

import parcel.application.request.ParcelRequest;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(4)
public class MediumParcel extends Parcel {

    private double rate = 0.04;
    private double maximumVolume = 2500;

    @Override
    public String name() {
        return "Medium Parcel";
    }

    @Override
    public boolean validate(ParcelRequest parcelRequest) {
        return parcelRequest.volume() < maximumVolume;
    }

    @Override
    public double getCost(ParcelRequest parcelRequest) {
        return parcelRequest.volume() * rate;
    }
}
