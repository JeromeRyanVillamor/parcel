package parcel.application;

import parcel.application.request.ParcelRequest;
import parcel.application.response.ParcelResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/parcel", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ParcelDeliveryCostController {

    @Autowired
    private ParcelDeliveryCost parcelDeliveryCost;

    @PostMapping("v1/delivery-cost")
    public ParcelResponse calculate(@Valid @RequestBody ParcelRequest parcelRequest) {
        return parcelDeliveryCost.compute(parcelRequest);
    }

}
