package parcel.application.response;

import java.util.Map;

public class ValidationErrorResponse {

    private Map<String, String> errors;
    private final String message = "The given data was invalid";

    public ValidationErrorResponse(Map<String, String> errors) {
        this.errors = errors;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }
}
