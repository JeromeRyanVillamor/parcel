package parcel.application.response;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class VoucherResponse {

    private String code;
    private double discount;
    private LocalDate expiry;
}
