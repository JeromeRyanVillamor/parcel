package parcel.application.response;

import lombok.Data;
import parcel.application.request.ParcelRequest;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Data
public class ParcelResponse {

    private ParcelRequest parcelRequest;
    private VoucherResponse voucherResponse;
    private double deliveryCost;
    private double discountedDeliveryCost;
    private String parcelType;

    public double getDeliveryCost() {
        return new BigDecimal(deliveryCost)
                    .setScale(2, RoundingMode.HALF_UP)
                    .doubleValue();
    }

    public double getDiscountedDeliveryCost() {
        double discount = voucherResponse == null ? 0 : voucherResponse.getDiscount();

        return new BigDecimal(deliveryCost - discount)
                .setScale(2, RoundingMode.HALF_UP)
                .doubleValue();
    }
}
