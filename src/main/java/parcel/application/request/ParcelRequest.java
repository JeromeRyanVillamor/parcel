package parcel.application.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ParcelRequest {

    @NotNull(message = "Weight must not be empty")
    private Double weight;

    @NotNull(message = "Height must not be empty")
    private Double height;

    @NotNull(message = "Width must not be empty")
    private Double width;

    @NotNull(message = "Length must not be empty")
    private Double length;

    private String voucherCode;

    public double volume() {
        return height * width * length;
    }
}
