package parcel.application.exception;

public class ParcelDeliveryCostException extends RuntimeException {

    public ParcelDeliveryCostException(String message) {
        super(message);
    }
}
