package parcel.application.exception;

public class MyntVoucherException extends RuntimeException {

    public MyntVoucherException(String message) {
        super(message);
    }
}
