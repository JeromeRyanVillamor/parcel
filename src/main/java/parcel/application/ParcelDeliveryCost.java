package parcel.application;

import parcel.application.exception.MyntVoucherException;
import parcel.application.exception.ParcelNotFoundException;
import parcel.application.parcels.Parcel;
import parcel.application.request.ParcelRequest;
import parcel.application.response.ParcelResponse;
import parcel.application.response.VoucherResponse;
import parcel.application.voucher.Voucher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ParcelDeliveryCost {

    @Autowired
    private List<Parcel> parcels;

    @Autowired
    private Voucher voucher;

    public ParcelResponse compute(ParcelRequest parcelRequest) {
        return parcels.stream()
                .filter(parcel -> parcel.validate(parcelRequest))
                .map(parcel -> toParcelResponse(parcelRequest, parcel))
                .findFirst()
                .orElseThrow(ParcelNotFoundException::new);
    }

    private ParcelResponse toParcelResponse(ParcelRequest parcelRequest, Parcel parcel) {
        ParcelResponse parcelResponse = new ParcelResponse();
        parcelResponse.setParcelRequest(parcelRequest);
        parcelResponse.setParcelType(parcel.name());
        parcelResponse.setDeliveryCost(parcel.getCost(parcelRequest));

        if(parcelRequest.getVoucherCode() != null) {
            parcelResponse.setVoucherResponse(this.fetchVoucher(parcelRequest.getVoucherCode()));
        }

        return parcelResponse;
    }

    private VoucherResponse fetchVoucher(String voucherCode) {
        VoucherResponse voucherResponse = voucher.fetch(voucherCode);

        if(voucherResponse.getExpiry().isBefore(LocalDate.now())) {
            throw new MyntVoucherException("Voucher code is already expired");
        }

        return voucherResponse;
    }
}
