package parcel.application.voucher;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.ClientCodecConfigurer;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import java.util.function.Consumer;

@Configuration
public class WebClientConfiguration {

    @Value("${mynt-base-url}")
    private String myntBaseUrl;

    @Bean
    public WebClient myntWebClient() {
        return this.builder()
                .baseUrl(myntBaseUrl)
                .build();
    }

    private WebClient.Builder builder() {
        Consumer<ClientCodecConfigurer> consumer = configurer ->
                configurer.defaultCodecs().enableLoggingRequestDetails(true);

        HttpClient httpClient = HttpClient.create()
                .tcpConfiguration(client -> client.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 2000)
                        .doOnConnected(conn -> conn
                                .addHandlerLast(new ReadTimeoutHandler(5))
                                .addHandlerLast(new WriteTimeoutHandler(5)
                                )));

        return WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .exchangeStrategies(strategies -> strategies.codecs(consumer));
    }
}
