package parcel.application.voucher;

import parcel.application.exception.MyntVoucherException;
import parcel.application.response.VoucherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class Voucher {

    @Autowired
    @Qualifier("myntWebClient")
    private WebClient webClient;

    @Value("${mynt-api-key}")
    private String apiKey;

    private static final String VOUCHER_ENDPOINT = "/voucher/";

    public VoucherResponse fetch(String voucher) {
        String endpoint = VOUCHER_ENDPOINT + voucher + "?key=" + apiKey;

        return webClient.get()
                .uri(endpoint)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, response -> Mono.error(new MyntVoucherException("Invalid voucher code")))
                .onStatus(HttpStatus::is5xxServerError, response -> Mono.error(new MyntVoucherException("Something went wrong when fetching voucher code")))
                .bodyToMono(VoucherResponse.class)
                .block();
    }
}
